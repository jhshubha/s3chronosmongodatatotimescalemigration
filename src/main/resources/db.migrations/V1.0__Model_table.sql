-- BEFORE CREATING TABLE
-- CREATE DATABASE timescale_poc;
-- \connect timescale_poc;
-- CREATE EXTENSION postgis;

CREATE TABLE tracking_data (
       source_id VARCHAR(256) NOT NULL,
       timestamp BIGINT NOT NULL,
       gps_timestamp BIGINT NOT NULL,
       location_description TEXT,
       data_client VARCHAR(256) NOT NULL,
       latitude DOUBLE PRECISION  NOT NULL,
       longitude DOUBLE PRECISION  NOT NULL,
       phone_number VARCHAR(15),
       speed DOUBLE PRECISION,
       fuel DOUBLE PRECISION,
       fuel_tank_one DOUBLE PRECISION,
       fuel_tank_two DOUBLE PRECISION,
       coolant DOUBLE PRECISION,
       engine_oil_pressure DOUBLE PRECISION,
       air_brake_pressure DOUBLE PRECISION,
       rpm DOUBLE PRECISION,
       battery_level DOUBLE PRECISION,
       message_type TEXT,
       x_axis DOUBLE PRECISION,
       y_axis DOUBLE PRECISION,
       z_axis DOUBLE PRECISION,
       payload JSONB,
       engine_status VARCHAR(256),
       gps_data_source_type VARCHAR(20),
       temperature_timestamp BIGINT,
       temperature_data_source_type VARCHAR(20),
       speed_category VARCHAR(20),
       ignition_status VARCHAR(20),
       engine_voltage DOUBLE PRECISION,
       actual_engine_status VARCHAR(20),
       fuel_smooth DOUBLE PRECISION,
       rpm_category VARCHAR(20),
       temperatures DOUBLE PRECISION[],
       average_temperature DOUBLE PRECISION,
       delta_distance DOUBLE PRECISION,
       delta_seconds INT,
       gps_fix BOOLEAN,
       sensor_power_status VARCHAR(20),
       packet_type VARCHAR(20),
       valid_data_point BOOLEAN,
       odometer DOUBLE PRECISION,
       point geometry(POINT,2163)
);

CREATE EXTENSION IF NOT EXISTS timescaledb CASCADE;
CREATE UNIQUE INDEX INDEX ON tracking_data (gps_timestamp DESC, source_id);
SELECT create_hypertable('tracking_data', 'gps_timestamp',chunk_time_interval => 86400000);

//latest entry query
select * from tracking_data where gps_timestamp > (extract('epoch' from now())::bigint)*1000 - 86400000  order by gps_timestamp desc limit 1;
