package com.rivigo.timescale;

import com.rivigo.timescale.service.Manager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
@Slf4j
public class TimescaleApplication {

  public static void main(String[] args) {
    ConfigurableApplicationContext run = SpringApplication.run(TimescaleApplication.class, args);
    Manager manager = run.getBean(Manager.class);
    manager.saveDataFromS3InDb();
    run.close();
  }
}
