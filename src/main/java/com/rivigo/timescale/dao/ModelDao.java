package com.rivigo.timescale.dao;

import com.rivigo.timescale.model.Model;
import java.util.List;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlBatch;

public interface ModelDao {

  @SqlBatch(
      "INSERT INTO tracking_data(source_id,timestamp,gps_timestamp,location_description,data_client,latitude,longitude,phone_number,speed,fuel,fuel_tank_one,fuel_tank_two,coolant,engine_oil_pressure,air_brake_pressure,rpm,battery_level,message_type, x_axis, y_axis, z_axis,payload,engine_status,gps_data_source_type,temperature_timestamp,temperature_data_source_type,speed_category,ignition_status,engine_voltage,actual_engine_status,fuel_smooth,rpm_category,temperatures,average_temperature,delta_distance,delta_seconds,gps_fix,sensor_power_status,packet_type,valid_data_point,odometer,point)"
          + "VALUES ( :sourceId, :timestamp, :gpsTimestamp, :locationDescription, :dataClient, :latitude, :longitude, :phoneNumber, :speed, :fuel, :fuelTankOne, :fuelTankTwo, :coolant, :engineOilPressure, :airBrakePressure,:rpm,:batteryLevel,:messageType,:xaxis,:yaxis,:zaxis,:payload,:engineStatus,:gpsDataSourceType,:temperatureTimestamp,:temperatureDataSourceType,:speedCategory,:ignitionStatus,:engineVoltage,:actualEngineStatus,:fuelSmooth,:rpmCategory,cast(:temperatures AS DOUBLE PRECISION[]),:averageTemperature,:deltaDistance,:deltaSeconds,:gpsFix,:sensorPowerStatus,:packetType,:validDataPoint,:odometer, ST_Transform(ST_SetSRID(ST_MakePoint(:longitude ,:latitude),4326),2163)) ON CONFLICT DO NOTHING")
  void batchAdd(@BindBean List<Model> modelList);
}
