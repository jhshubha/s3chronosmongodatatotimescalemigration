package com.rivigo.timescale.service;

import java.io.IOException;
import java.util.List;
import org.json.JSONObject;

public interface DataReader {
  List<JSONObject> getNextBatch() throws IOException;

  int getBatchSize();
}
