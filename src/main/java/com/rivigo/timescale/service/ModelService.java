package com.rivigo.timescale.service;

import com.rivigo.timescale.dao.ModelDao;
import com.rivigo.timescale.model.Model;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ModelService {

  private ModelDao modelDao;

  @Autowired
  public ModelService(ModelDao modelDao) {
    this.modelDao = modelDao;
  }

  public void insertModels(List<Model> modelList) {
    modelDao.batchAdd(modelList);
  }
}
