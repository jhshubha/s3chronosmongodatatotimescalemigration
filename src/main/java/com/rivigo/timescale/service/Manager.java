package com.rivigo.timescale.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class Manager {

  private String startDate;
  private String endDate;
  private SaveS3DataInDb saveS3DataInDb;
  private int numberOfDays;
  private String bucketKeyFormat;
  private String bucketName;

  @Autowired
  public Manager(
      @Value("${s3.startDate}") String startDate,
      @Value("${s3.endDate}") String endDate,
      SaveS3DataInDb saveS3DataInDb,
      @Value("${s3.numberOfDays}") int numberOfDays,
      @Value("${s3.bucketKeyFormat}") String bucketKeyFormat,
      @Value("${s3.bucketName}") String bucketName) {
    this.startDate = startDate;
    this.endDate = endDate;
    this.saveS3DataInDb = saveS3DataInDb;
    this.numberOfDays = numberOfDays;
    this.bucketKeyFormat = bucketKeyFormat;
    this.bucketName = bucketName;
    log.info(
        "Values in manager, startDate: {}, endDate: {}, numberOfDays: {}, bucketKeyFormat: ({}), bucketName: {}",
        startDate,
        endDate,
        numberOfDays,
        bucketKeyFormat,
        bucketName);
  }

  public void saveDataFromS3InDb() {

    String pattern = "yyyy-MM-dd";
    DateFormat dateFormat = new SimpleDateFormat(pattern);
    String currentDate = startDate;
    for (int i = 0; i < numberOfDays; i++) {
      if (endDate.compareTo(currentDate) < 0) {
        log.info("processed up till : {}", endDate);
        log.info("Now exiting manager");
        break;
      }
      Calendar calendar = Calendar.getInstance();
      try {
        Date date = dateFormat.parse(currentDate);
        calendar.setTime(date);
        String bucketKey = String.format(bucketKeyFormat, currentDate, currentDate);
        log.info(
            "calling save data for date: {} bucket key : {} and bucket name: {}",
            date,
            bucketKey,
            bucketName);
        saveS3DataInDb.saveDate(bucketName, bucketKey);
        calendar.add(Calendar.DATE, 1);
        currentDate = dateFormat.format(calendar.getTime());
      } catch (ParseException e) {
        log.info("Unable to parse date : {}", currentDate);
      }
    }
  }
}
