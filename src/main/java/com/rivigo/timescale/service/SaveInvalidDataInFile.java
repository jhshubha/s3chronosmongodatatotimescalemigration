package com.rivigo.timescale.service;

import java.io.*;
import javax.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SaveInvalidDataInFile {

  private final PrintWriter printWriter;
  private final boolean writeToErrorFile;

  @Autowired
  public SaveInvalidDataInFile(
      @Value("${error.file}") String errorFilePath,
      @Value("${write.to.error.file}") boolean writeToErrorFile)
      throws IOException {
    log.info(
        "Will write invalid json objects to file : {}, writeToErrorFile: {}",
        errorFilePath,
        writeToErrorFile);
    this.writeToErrorFile = writeToErrorFile;
    File file = new File(errorFilePath);
    printWriter = new PrintWriter(new BufferedWriter(new FileWriter(file, true)));
  }

  public void addJsonObjectToFile(JSONObject jsonObject) {
    if (writeToErrorFile) {
      printWriter.print("json object -> ");
      printWriter.println(jsonObject);
    }
  }

  public void addStringToFile(String string) {
    if (writeToErrorFile) {
      printWriter.print("string : -> ");
      printWriter.println(string);
    }
  }

  @PreDestroy
  public void destroy() {
    log.info("Pre destory called for save invalid data in file");
    printWriter.flush();
    printWriter.close();
    log.info("Successfully closed printwriter opened for error file");
  }
}
