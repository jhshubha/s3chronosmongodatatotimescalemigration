package com.rivigo.timescale.service;

import com.rivigo.timescale.converter.JsonToModelConverter;
import com.rivigo.timescale.model.Model;
import java.io.*;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class SaveDataInDb {

  private final ModelService modelService;
  private final JsonToModelConverter jsonToModelConverter;

  @Autowired
  public SaveDataInDb(ModelService modelService, JsonToModelConverter jsonToModelConverter) {
    this.modelService = modelService;
    this.jsonToModelConverter = jsonToModelConverter;
  }

  public void saveData(List<JSONObject> nextBatch) {
    if (nextBatch == null) {
      log.error("got a null batch to process, ignoring");
    }
    List<Model> modelList =
        nextBatch.stream()
            .map(jsonToModelConverter::convertToModel)
            .filter(model -> null != model)
            .collect(Collectors.toList());
    modelService.insertModels(modelList);
  }
}
