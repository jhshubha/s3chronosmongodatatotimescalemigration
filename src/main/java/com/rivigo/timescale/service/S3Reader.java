package com.rivigo.timescale.service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.annotation.PreDestroy;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

// @Component
@Slf4j
public class S3Reader implements DataReader {

  private AmazonS3 s3Client;
  private final int BATCH_SIZE = 1500;
  private static int noOfKeys = 1;
  private String bucketName;
  private String date;
  private String bucketKey;
  private SaveInvalidDataInFile saveInvalidDataInFile;
  private static BufferedReader reader;
  private static S3Object fullObject;

  @Autowired
  public S3Reader(
      @Value("${s3.clientRegion}") String clientRegion,
      @Value("${s3.bucketName}") String bucketName,
      @Value("${s3.key}") String key,
      @Value("${s3.startDate}") String date,
      SaveInvalidDataInFile saveInvalidDataInFile) {
    AWSCredentials credentials =
        new BasicAWSCredentials("AKIAJEOANCS3IWZZNSUQ", "bJMkJs92/4DqLcOqJaclt6vEb5NYmSq+wEKlUzDt");
    this.bucketName = bucketName;
    this.bucketKey = key;
    this.saveInvalidDataInFile = saveInvalidDataInFile;
    this.date = date;
    s3Client =
        AmazonS3ClientBuilder.standard()
            .withRegion(clientRegion)
            .withCredentials(new AWSStaticCredentialsProvider(credentials))
            .build();

    // Get an object and print its contents.
    log.info("bucket name: {}, bucket key for start : {}", bucketName, bucketKey);
    System.out.println("Downloading an object");
    boolean flag = true;
    while (flag) {
      try {
        System.out.println(s3Client.getS3AccountOwner().getDisplayName());
        fullObject = s3Client.getObject(new GetObjectRequest(bucketName, bucketKey));
        flag = false;
      } catch (Exception e) {
        System.out.println("Retry");
      }
    }
    fullObject = s3Client.getObject(new GetObjectRequest(bucketName, bucketKey));

    System.out.println("Content-Type: " + fullObject.getObjectMetadata().getContentType());
    reader = new BufferedReader(new InputStreamReader(fullObject.getObjectContent()));
  }

  @Override
  public List<JSONObject> getNextBatch() throws IOException {

    return getJSONfromS3InBatch(BATCH_SIZE);
  }

  @Override
  public int getBatchSize() {
    return BATCH_SIZE;
  }

  @PreDestroy
  public void destroy() throws IOException {
    log.info("Shutting down s3 client, closing all connections");
    fullObject.close();
    s3Client.shutdown();
    //    close open connections here
  }

  private List<JSONObject> getJSONfromS3InBatch(int batchSize) throws IOException {
    // Read the text input stream one line at a time and display each line.
    int i = 0;
    String line;
    List<JSONObject> jsonObjects = new ArrayList<>();
    Boolean areLinesPresent = false;

    while ((line = reader.readLine()) != null && i < batchSize) {
      areLinesPresent = true;
      try {
        jsonObjects.add(new JSONObject(line));
      } catch (Exception ex) {
        log.error("from s3 received -> {}", line);
        log.error("exception occured -> ", ex);
        saveInvalidDataInFile.addStringToFile(line);
      }
      i++;
    }

    if (noOfKeys < 60 && areLinesPresent.equals(false)) {
      String pattern = "yyyy-MM-dd";
      DateFormat simpleDateFormat = new SimpleDateFormat(pattern);
      Date date1 = null;
      try {
        date1 = simpleDateFormat.parse(date);
      } catch (ParseException e) {
        log.error("exception -> ", e);
        log.error("Cannot parse " + date + " to form " + pattern);
      }
      Calendar c = Calendar.getInstance();
      c.setTime(date1);
      c.add(Calendar.DATE, 1);
      date = simpleDateFormat.format(c.getTime());

      bucketKey =
          String.format(
              "processed-sensor-data-day-wise/dt=%s/processed_sensor_data-%s.json", date, date);
      System.out.println(bucketKey);
      fullObject.close();
      boolean flag = true;
      while (flag) {
        try {
          fullObject = s3Client.getObject(new GetObjectRequest(bucketName, bucketKey));
          flag = false;
        } catch (Exception e) {
          System.out.println("Retry");
        }
      }
      reader = new BufferedReader(new InputStreamReader(fullObject.getObjectContent()));
      noOfKeys++;
      while ((line = reader.readLine()) != null && i < batchSize) {
        jsonObjects.add(new JSONObject(line));
        i++;
      }
      System.out.println(noOfKeys);
    }
    return jsonObjects;
  }
}
