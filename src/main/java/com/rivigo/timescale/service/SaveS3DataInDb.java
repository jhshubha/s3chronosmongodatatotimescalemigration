package com.rivigo.timescale.service;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SaveS3DataInDb {
  private String clientRegion;
  private String accessKey;
  private String secretKey;
  private int batchSize;
  private SaveDataInDb saveDataInDb;
  private SaveInvalidDataInFile saveInvalidDataInFile;

  @Autowired
  public SaveS3DataInDb(
      @Value("${s3.clientRegion}") String clientRegion,
      @Value("${s3.accessKey}") String accessKey,
      @Value("${s3.secretKey}") String secretKey,
      @Value("${db.batch.save.size}") int batchSize,
      SaveDataInDb saveDataInDb,
      SaveInvalidDataInFile saveInvalidDataInFile) {
    this.clientRegion = clientRegion;
    this.accessKey = accessKey;
    this.secretKey = secretKey;
    this.batchSize = batchSize;
    this.saveDataInDb = saveDataInDb;
    this.saveInvalidDataInFile = saveInvalidDataInFile;
  }

  @Async
  public void saveDate(String bucketName, String bucketKey) {

    AWSCredentials credentials = new BasicAWSCredentials(accessKey, secretKey);
    log.info("saving data for bucketName: {}, bucketKey: {}", bucketName, bucketKey);
    AmazonS3 s3Client = null;
    S3Object s3Object = null;
    BufferedReader reader = null;
    String line;
    try {
      s3Client =
          AmazonS3ClientBuilder.standard()
              .withRegion(clientRegion)
              .withCredentials(new AWSStaticCredentialsProvider(credentials))
              .build();
      s3Object = s3Client.getObject(new GetObjectRequest(bucketName, bucketKey));
      reader = new BufferedReader(new InputStreamReader(s3Object.getObjectContent()));
      log.info("Downloading content for bucketName: {}, bucketKey: {}", bucketName, bucketKey);
      List<JSONObject> jsonObjectList = new ArrayList<>();
      long linesProcessed = (long) 0;
      while ((line = reader.readLine()) != null) {
        try {
          jsonObjectList.add(new JSONObject(line));
          linesProcessed++;
        } catch (JSONException e) {
          log.info("Unable to parse line : {} into json", line);
          saveInvalidDataInFile.addStringToFile(line);
        }
        if (jsonObjectList.size() == batchSize) {
          saveDataInDb.saveData(new ArrayList<>(jsonObjectList));
          jsonObjectList.clear();
          log.info(
              "processed {} lines for bucketKey: {}, bucketName: {}",
              linesProcessed,
              bucketKey,
              bucketName);
        }
      }
      if (jsonObjectList.size() > 0) {
        saveDataInDb.saveData(new ArrayList<>(jsonObjectList));
      }
      log.info(
          "processed {} lines for bucketKey: {}, bucketName: {}",
          linesProcessed,
          bucketKey,
          bucketName);
      log.info("Done processing bucketKey: {}, bucketName: {}", bucketKey, bucketName);
    } catch (Exception e) {
      log.error(
          "Exception occured in parsing file from s3 with bucketName: {}, bucketKey: {}",
          bucketName,
          bucketKey);
      log.error("Exception : ", e);
    } finally {
      if (reader != null) {
        try {
          reader.close();
          log.info("successfully closed buffer reader reading data from s3");
        } catch (IOException e) {
          log.error("error closing buffered reader: ", reader);
          log.error("Exception : ", e);
        }
      }
      if (s3Object != null) {
        try {
          s3Object.close();
          log.info("successfully closed s3 object");
        } catch (IOException e) {
          log.error("Error closing s3 object: {}", s3Object);
          log.error("Exception : ", e);
        }
      }
      if (s3Client != null) {
        s3Client.shutdown();
        log.info("successfully closed s3Client");
      }
    }
  }
}
