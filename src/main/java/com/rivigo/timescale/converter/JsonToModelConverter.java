package com.rivigo.timescale.converter;

import com.rivigo.timescale.model.Model;
import com.rivigo.timescale.service.SaveInvalidDataInFile;
import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JsonToModelConverter {

  private Map<String, Integer> isPresent = new HashMap<>();
  private final SaveInvalidDataInFile saveInvalidDataInFile;
  private final boolean writeToErrorFile;

  @Autowired
  public JsonToModelConverter(
      SaveInvalidDataInFile saveInvalidDataInFile,
      @Value("${write.to.error.file}") boolean writeToErrorFile) {
    this.saveInvalidDataInFile = saveInvalidDataInFile;
    this.writeToErrorFile = writeToErrorFile;
    Field[] allFields = Model.class.getDeclaredFields();

    for (Field field : allFields) {
      isPresent.put(field.getName(), 1);
    }
  }

  public Model convertToModel(JSONObject jsonObject) {
    Integer deltaSeconds;
    Boolean gpsFix, validDataPoint;
    List<Double> temperatures = new ArrayList<>();
    Timestamp gpsTimestamp, timestamp, temperatureTimestamp;
    String sourceId,
        gpsDataSourceType,
        packetType,
        sensorPowerStatus,
        dataClient,
        phoneNumber,
        messageType,
        temperatureDataSourceType,
        speedCategory = null,
        engineStatus,
        ignitionStatus,
        actualEngineStatus,
        rpmCategory,
        locationDesciption;
    Double locationLongitude,
        locationLatitude,
        deltaDistance,
        speed,
        averageTemperature,
        fuel,
        fuelTankOne,
        fuelTankTwo,
        coolant,
        engineOilPressure,
        airBrakePressure,
        rpm,
        batteryLevel,
        xAxis,
        yAxis,
        engineVoltage,
        odometer,
        fuelSmooth,
        zAxis;
    JSONObject payload = new JSONObject();

    try {
      JSONObject location = (JSONObject) jsonObject.get("location");
      JSONArray coordinates = location.getJSONArray("coordinates");
      locationLongitude = (Double) coordinates.get(0);
      locationLatitude = (Double) coordinates.get(1);
      // TODO:
      try {
        speed = (jsonObject.has("speed")) ? (Double) jsonObject.get("speed") : null;
      } catch (Exception e) {
        speed = null;
      }
      fuel = (jsonObject.has("fuel")) ? Double.parseDouble(jsonObject.getString("fuel")) : null;
      locationDesciption =
          (jsonObject.has("locationDescription"))
              ? jsonObject.getString("locationDescription")
              : null;
      fuelTankOne =
          (jsonObject.has("fuelTankOne"))
              ? Double.parseDouble(jsonObject.getString("fuelTankOne"))
              : null;
      fuelTankTwo =
          (jsonObject.has("fuelTankTwo"))
              ? Double.parseDouble(jsonObject.getString("fuelTankTwo"))
              : null;
      coolant =
          (jsonObject.has("coolant")) ? Double.parseDouble(jsonObject.getString("coolant")) : null;
      engineOilPressure =
          (jsonObject.has("engineOilPressure"))
              ? Double.parseDouble(jsonObject.getString("engineOilPressure"))
              : null;
      airBrakePressure =
          (jsonObject.has("airBrakePressure"))
              ? Double.parseDouble(jsonObject.getString("airBrakePressure"))
              : null;
      rpm = (jsonObject.has("rpm")) ? Double.parseDouble(jsonObject.getString("rpm")) : null;
      batteryLevel =
          (jsonObject.has("batteryLevel"))
              ? Double.parseDouble(jsonObject.getString("batteryLevel"))
              : null;
      xAxis = (jsonObject.has("xAxis")) ? (Double) jsonObject.get("xAxis") : null;
      yAxis = (jsonObject.has("yAxis")) ? (Double) jsonObject.get("yAxis") : null;
      zAxis = (jsonObject.has("zAxis")) ? (Double) jsonObject.get("zAxis") : null;
      engineVoltage =
          (jsonObject.has("engineVoltage"))
              ? Double.parseDouble(jsonObject.getString("engineVoltage"))
              : null;
      fuelSmooth = (jsonObject.has("fuelSmooth")) ? (Double) jsonObject.get("fuelSmooth") : null;
      averageTemperature =
          (jsonObject.has("averageTemperature"))
              ? Double.parseDouble(jsonObject.getString("averageTemperature"))
              : null;
      deltaDistance =
          (jsonObject.has("deltaDistance")) ? (Double) jsonObject.get("deltaDistance") : null;
      deltaSeconds =
          (jsonObject.has("deltaSeconds")) ? (Integer) jsonObject.get("deltaSeconds") : null;
      gpsFix = (jsonObject.has("gpsFix")) ? (Boolean) jsonObject.get("gpsFix") : null;
      validDataPoint =
          (jsonObject.has("validDataPoint")) ? (Boolean) jsonObject.get("validDataPoint") : null;
      odometer = (jsonObject.has("odometer")) ? (Double) jsonObject.get("odometer") : null;
      phoneNumber = (jsonObject.has("phoneNumber")) ? jsonObject.getString("phoneNumber") : null;
      messageType = (jsonObject.has("messageType")) ? jsonObject.getString("messageType") : null;
      payload = (jsonObject.has("payload")) ? jsonObject.getJSONObject("payload") : payload;
      temperatureDataSourceType =
          (jsonObject.has("temperatureDataSourceType"))
              ? jsonObject.getString("temperatureDataSourceType")
              : null;
      speedCategory =
          (jsonObject.has("speedCategory")) ? jsonObject.getString("speedCategory") : null;
      engineStatus = (jsonObject.has("engineStatus")) ? jsonObject.getString("engineStatus") : null;
      ignitionStatus =
          (jsonObject.has("ignitionStatus")) ? jsonObject.getString("ignitionStatus") : null;
      actualEngineStatus =
          (jsonObject.has("actualEngineStatus"))
              ? jsonObject.getString("actualEngineStatus")
              : null;
      rpmCategory = (jsonObject.has("rpmCategory")) ? jsonObject.getString("rpmCategory") : null;
      sensorPowerStatus =
          (jsonObject.has("sensorPowerStatus")) ? jsonObject.getString("sensorPowerStatus") : null;
      packetType = (jsonObject.has("packetType")) ? jsonObject.getString("packetType") : null;
      temperatureTimestamp =
          (jsonObject.has("temperatureTimestamp"))
              ? getDateKeyAsTimestamp((JSONObject) jsonObject.get("temperatureTimestamp"))
              : null;
      if (jsonObject.has("temperatures")) {
        JSONArray jsonArray = jsonObject.getJSONArray("temperatures");

        for (int i = 0; i < jsonArray.length(); i++) {
          temperatures.add(Double.parseDouble(jsonArray.getString(i)));
        }
        if (jsonArray.length() > 0) {
          System.out.println("JSONArray " + jsonArray.toString());
          System.out.println(temperatures.toString());
        }
      }

      Long tmpTimestamp;
      if (temperatureTimestamp == null) {
        tmpTimestamp = null;
      } else {
        tmpTimestamp = temperatureTimestamp.getTime();
      }

      timestamp = getDateKeyAsTimestamp((JSONObject) jsonObject.get("timestamp"));
      gpsTimestamp = getDateKeyAsTimestamp((JSONObject) jsonObject.get("gpsTimestamp"));
      gpsDataSourceType = jsonObject.getString("gpsDataSourceType");
      dataClient = jsonObject.getString("dataClient");
      sourceId = jsonObject.getString("sourceId");

      return Model.builder()
          .gpsTimestamp(gpsTimestamp.getTime())
          .timestamp(timestamp.getTime())
          .temperatureTimestamp(tmpTimestamp)
          .locationDescription(locationDesciption)
          .sourceId(sourceId)
          .phoneNumber(phoneNumber)
          .gpsDataSourceType(gpsDataSourceType)
          .dataClient(dataClient)
          .latitude(locationLatitude)
          .longitude(locationLongitude)
          .speed(speed)
          .batteryLevel(batteryLevel)
          .fuel(fuel)
          .fuelSmooth(fuelSmooth)
          .fuelTankOne(fuelTankOne)
          .fuelTankTwo(fuelTankTwo)
          .xaxis(xAxis)
          .yaxis(yAxis)
          .zaxis(zAxis)
          .coolant(coolant)
          .rpm(rpm)
          .validDataPoint(validDataPoint)
          .gpsFix(gpsFix)
          .engineVoltage(engineVoltage)
          .deltaDistance(deltaDistance)
          .deltaSeconds(deltaSeconds)
          .averageTemperature(averageTemperature)
          .airBrakePressure(airBrakePressure)
          .temperatures(temperatures)
          .odometer(odometer)
          .speedCategory(speedCategory)
          .messageType(messageType)
          .packetType(packetType)
          .payload(payload)
          .temperatureDataSourceType(temperatureDataSourceType)
          .engineOilPressure(engineOilPressure)
          .engineStatus(engineStatus)
          .ignitionStatus(ignitionStatus)
          .actualEngineStatus(actualEngineStatus)
          .sensorPowerStatus(sensorPowerStatus)
          .rpmCategory(rpmCategory)
          .build();
    } catch (Exception excption) {
      log.info("exception: {}", excption);
      log.info("JSON object -> {}", jsonObject);
      if (writeToErrorFile) saveInvalidDataInFile.addJsonObjectToFile(jsonObject);
      return null;
    }
  }

  private Timestamp getDateKeyAsTimestamp(JSONObject jsonObject) throws ParseException {
    String value = jsonObject.get("$date").toString();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    Date date = dateFormat.parse(value);
    return new Timestamp(date.getTime());
  }
}
