package com.rivigo.timescale.model;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.json.JSONObject;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class Model {
  String sourceId;
  Long timestamp;
  Long gpsTimestamp;
  String locationDescription;
  String dataClient;
  Double latitude;
  Double longitude;
  String phoneNumber;
  Double speed;
  Double fuel;
  Double fuelTankOne;
  Double fuelTankTwo;
  Double coolant;
  Double engineOilPressure;
  Double airBrakePressure;
  Double rpm;
  Double batteryLevel;
  String messageType;
  Double xaxis;
  Double yaxis;
  Double zaxis;
  JSONObject payload;
  String gpsDataSourceType;
  Long temperatureTimestamp;
  String temperatureDataSourceType;
  String speedCategory;
  String engineStatus;
  String ignitionStatus;
  Double engineVoltage;
  String actualEngineStatus;
  Double fuelSmooth;
  String rpmCategory;
  List<Double> temperatures;
  Double averageTemperature;
  Double deltaDistance;
  Integer deltaSeconds;
  Boolean gpsFix;
  String sensorPowerStatus;
  String packetType;
  Boolean validDataPoint;
  Double odometer;
}
