package com.rivigo.timescale.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.json.JSONObject;
import org.postgresql.util.PGobject;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@Slf4j
public class JdbiConfig {

  @Bean
  public Jdbi jdbi(Environment environment) {
    HikariConfig dataSource = new HikariDataSource();
    dataSource.setAutoCommit(true);
    //    USING POSTGRES
    dataSource.setDriverClassName("org.postgresql.Driver");
    dataSource.setJdbcUrl(environment.getProperty("timescale.db.master.url"));
    dataSource.setUsername(environment.getProperty("timescale.db.master.user"));
    dataSource.setPassword(environment.getProperty("timescale.db.master.password"));
    dataSource.setIdleTimeout(
        Long.parseLong(
            environment.getProperty("timescale.db.master.idleConnectionTestPeriodInSeconds")));
    dataSource.setMaximumPoolSize(
        Integer.parseInt(
            environment.getProperty("timescale.db.master.maxConnectionsPerPartition")));
    dataSource.setLeakDetectionThreshold(
        Long.parseLong(environment.getProperty("timescale.db.master.leakDetectionThreshold")));
    dataSource.addDataSourceProperty(
        "useLocalSessionState",
        environment.getProperty("timescale.db.master.useLocalSessionState"));
    dataSource.addDataSourceProperty(
        "cachePrepStmts", environment.getProperty("timescale.db.master.cachePrepStmts"));
    dataSource.addDataSourceProperty(
        "useServerPrepStmts", environment.getProperty("timescale.db.master.useServerPrepStmts"));
    dataSource.addDataSourceProperty(
        "prepStmtCacheSize", environment.getProperty("timescale.db.master.prepStmtCacheSize"));
    dataSource.addDataSourceProperty(
        "prepStmtCacheSqlLimit",
        environment.getProperty("timescale.db.master.prepStmtCacheSqlLimit"));
    dataSource.addDataSourceProperty(
        "useUsageAdvisor", environment.getProperty("timescale.db.master.useUsageAdvisor"));
    HikariDataSource hikariDataSource = new HikariDataSource(dataSource);
    Jdbi jdbi = Jdbi.create(hikariDataSource);
    jdbi.installPlugin(new SqlObjectPlugin());
    jdbi.registerArgument(new JsonBlobFactory());
    jdbi.registerArrayType(Double.class, "float");
    jdbi.registerColumnMapper(
        JSONObject.class,
        (rs, col, ctx) -> {
          PGobject pgObject = (PGobject) rs.getObject(col);
          if (pgObject != null) return new JSONObject(pgObject.getValue());
          return null;
        });
    return jdbi;
  }
}
