package com.rivigo.timescale.config;

import java.util.concurrent.Executor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
@Slf4j
public class ExecutorServiceConfig implements AsyncConfigurer {

  private final int corePoolSize;
  private final int maxPoolSize;
  private final int queueCapacity;

  public ExecutorServiceConfig(
      @Value("${thread.corePoolSize}") int corePoolSize,
      @Value("${thread.maxPoolSize}") int maxPoolSize,
      @Value("${thread.queueCapacity}") int queueCapacity) {
    this.corePoolSize = corePoolSize;
    this.maxPoolSize = maxPoolSize;
    this.queueCapacity = queueCapacity;
  }

  @Override
  public Executor getAsyncExecutor() {
    ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
    executor.setCorePoolSize(corePoolSize);
    executor.setMaxPoolSize(maxPoolSize);
    executor.setQueueCapacity(queueCapacity);
    executor.setThreadNamePrefix("TbDb Executor-");
    executor.initialize();
    return executor;
  }
}
