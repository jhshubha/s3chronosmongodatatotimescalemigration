package com.rivigo.timescale.config;

import java.sql.Types;
import org.jdbi.v3.core.argument.AbstractArgumentFactory;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.config.ConfigRegistry;
import org.json.JSONObject;
import org.postgresql.util.PGobject;

public class JsonBlobFactory extends AbstractArgumentFactory<JSONObject> {
  public JsonBlobFactory() {
    super(Types.BLOB);
  }

  @Override
  protected Argument build(JSONObject value, ConfigRegistry config) {
    return ((position, statement, ctx) -> {
      PGobject dataObject = new PGobject();
      dataObject.setType("jsonb");
      dataObject.setValue(value.toString());
      statement.setObject(position, dataObject);
    });
  }
}
