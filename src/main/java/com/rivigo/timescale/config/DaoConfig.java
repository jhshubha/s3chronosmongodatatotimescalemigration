package com.rivigo.timescale.config;

import com.rivigo.timescale.dao.ModelDao;
import lombok.extern.slf4j.Slf4j;
import org.jdbi.v3.core.Jdbi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class DaoConfig {

  @Bean
  public ModelDao modelDao(Jdbi jdbi) {
    return jdbi.onDemand(ModelDao.class);
  }
}
